package ust.hk.flanker.fyp.plugin;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ust.hk.flanker.fyp.AppContext;
import ust.hk.flanker.fyp.DbgUtil;
import ust.hk.flanker.fyp.Helper;
import ust.hk.flanker.fyp.filter.EditTextToStringTaintFilter;
import ust.hk.flanker.fyp.filter.PassViewSeedFilter;
import ust.hk.flanker.fyp.filter.GeneralResultFilter;
import ust.hk.flanker.fyp.filter.SeedFilter;
import ust.hk.flanker.fyp.filter.StringToBytesTaintFilter;
import ust.hk.flanker.fyp.filter.TaintFilter;
import ust.hk.flanker.fyp.filter.TextViewGetTextTaintFilter;
import ust.hk.flanker.fyp.util.LayoutParser;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Slicer;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.collections.HashSetMultiMap;
import com.ibm.wala.util.collections.MultiMap;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.intset.IntSet;

public class PasswordAnalysisPlugin extends AnalysisPlugin {
	private static Logger logger = LogManager
			.getLogger(PasswordAnalysisPlugin.class);
	private Queue<Statement> _viewSeeds = new LinkedList<Statement>();
	private Queue<Statement> _prefSeeds = new LinkedList<Statement>();
	private Set<Integer> _passViewIds = new HashSet<Integer>();
	private Map<Statement, Collection<InstanceKey>> _seedInstanceKeysMapping = new HashMap<Statement, Collection<InstanceKey>>();

	@Override
	protected void doSlice(AppContext data) {
		GeneralResultFilter filter = new GeneralResultFilter();
		StringToBytesTaintFilter toBytesTaintFilter = new StringToBytesTaintFilter();
		while (!_viewSeeds.isEmpty()) {
			Statement seedStatement = _viewSeeds.poll();
			logger.info("seed statement: {}", seedStatement);
			// DbgUtil.contextPrint(statement);
			Collection<Statement> slices = data._slicer.computeForwardThinSlice(seedStatement);
			for (Iterator<Statement> iterator = slices.iterator(); iterator
					.hasNext();) {
				Statement s = (Statement) iterator.next();
				if (toBytesTaintFilter.accept(s, seedStatement)) {
					Statement toadd = Helper.getReturnStatementForCall(s);
					_viewSeeds.add(toadd);
					_seedInstanceKeysMapping.put(
							toadd,
							Helper.getReturnInstanceKeys(
									s,
									data.dma));
				}
				else if (filter.accept(s)) {
					if (s.toString().contains("SharedPreference")
							&& s.toString().contains("putString")) {
						Collection<InstanceKey> seedInstanceKeys = _seedInstanceKeysMapping
								.get(seedStatement);
						
						Collection<InstanceKey> arg1InstanceKeys = Helper
								.getArgInstanceKeys(s, data.dma, 1);
						Collection<InstanceKey> arg2InstanceKeys = Helper
								.getArgInstanceKeys(s, data.dma, 2);
						/*
						for (InstanceKey instanceKey : seedInstanceKeys) {
							System.out.println(instanceKey);
						}
						for (InstanceKey instanceKey : arg1InstanceKeys) {
							System.out.println(instanceKey);
						}
						for (InstanceKey instanceKey : arg2InstanceKeys) {
							System.out.println(instanceKey);
						}*/
						// do a heuristic guess
						if (arg2InstanceKeys.size() == seedInstanceKeys.size()) {
							setResult(AnalysisResult.CONFIRM);
							_resultMapping.put(AnalysisResult.CONFIRM,
									Pair.make(seedStatement, s));
						} else {
							// guess arg 1, does it contains "password"?
							for (InstanceKey instanceKey : arg1InstanceKeys) {
								if (instanceKey.toString().toLowerCase()
										.contains("password")) {
									setResult(AnalysisResult.CONFIRM);// dont
																		// worry,
																		// it
																		// won't
																		// replace
																		// high-level
																		// alert
									_resultMapping.put(AnalysisResult.CONFIRM,
											Pair.make(seedStatement, s));
								} else {
									if (instanceKey.toString().toLowerCase()
											.contains("pass")
											|| instanceKey.toString()
													.toLowerCase()
													.contains("pw")) {
										setResult(AnalysisResult.POSITIVE);
										_resultMapping.put(
												AnalysisResult.POSITIVE,
												Pair.make(seedStatement, s));
									} else {
										/*
										 * setResult(AnalysisResult.NEGATIVE);
										 * _resultMapping
										 * .put(AnalysisResult.NEGATIVE,
										 * Pair.make(seedStatement, s));
										 */
									}
								}
							}
						}

					} else if (s.toString().contains("PrintStream")
							&& s.toString().contains("print")) {
						// monitor system.out.println
						Collection<InstanceKey> seedInstanceKeys = _seedInstanceKeysMapping
								.get(seedStatement);
						// DbgUtil.printArgInstanceKey(s, data.dma);
						Collection<InstanceKey> arg1InstanceKeys = Helper
								.getArgInstanceKeys(s, data.dma, 1);
						// do a heuristic guess
						if (arg1InstanceKeys.size() == seedInstanceKeys.size()) {
							setResult(AnalysisResult.CONFIRM);
							_resultMapping.put(AnalysisResult.CONFIRM,
									Pair.make(seedStatement, s));
						} else {
							setResult(AnalysisResult.POSITIVE);
							_resultMapping.put(AnalysisResult.POSITIVE,
									Pair.make(seedStatement, s));
						}
					}

				}
				// contextPrint(s);
			}
		}
	}

	private void doInitViewIds(AppContext data) {
		try {
			_passViewIds = LayoutParser.getPasswordInputIds(new File(
			// "/Users/hqdvista/androidtools/com.tencent.mm-1/res/layout"
			// "/Users/hqdvista/androidtools/mycc98/res/layout"
			// "/Users/hqdvista/androidtools/shuo_android_a23-1/res/layout"
			// "/Users/hqdvista/Documents/hklab/android/casestudy/login.xml"
					data.unpackedPath + "res/"
			// "/Users/hqdvista/Documents/hklab/android/casestudy/androidtest/res/layout/activity_main.xml"
					));
			logger.info(_passViewIds);
		} catch (Exception e) {
			logger.error("unable to init pass view from xml");
			e.printStackTrace();
		}
	}

	private void doFoundString(AppContext data) {
		SeedFilter viewFilter = null;
		TaintFilter getTextFilter = null;
		EditTextToStringTaintFilter editTextToStringTaintFilter = new EditTextToStringTaintFilter();

		if (_passViewIds != null && _passViewIds.size() != 0) {
			viewFilter = new PassViewSeedFilter(_passViewIds);
			getTextFilter = new TextViewGetTextTaintFilter();
		}

		for (Iterator<? extends CGNode> it = data._cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			IR ir = n.getIR();
			if (ir == null) {
				continue;
			}
			for (Iterator<SSAInstruction> sit = ir.iterateAllInstructions(); sit
					.hasNext();) {

				SSAInstruction s = sit.next();
				// process findViewById related
				if (viewFilter != null && viewFilter.accept(n, s)) {
					IntSet indices = ir
							.getCallInstructionIndices(((SSAInvokeInstruction) s)
									.getCallSite());
					Assertions.productionAssertion(indices.size() == 1,
							"expected 1 but got " + indices.size());
					Statement viewReturnStatement = Helper
							.getReturnStatementForCall(new NormalStatement(n,
									indices.intIterator().next()));
					logger.debug(viewReturnStatement);
					Collection<Statement> slices = data._slicer
							.computeForwardThinSlice(viewReturnStatement);
					// we now found findViewByID
					for (Iterator<Statement> iterator = slices.iterator(); iterator
							.hasNext();) {
						Statement viewUseStatement = (Statement) iterator
								.next();
						if (getTextFilter != null
								&& getTextFilter.accept(viewUseStatement,
										viewReturnStatement)) {
							// we now found getText
							Collection<Statement> editableUseSlices = data._slicer
									.computeForwardThinSlice(Helper
											.getReturnStatementForCall(viewUseStatement));
							for (Iterator<Statement> eit = editableUseSlices
									.iterator(); eit.hasNext();) {
								Statement editableUseStatement = eit.next();
								// contextPrint(editableUseStatement);
								if (editTextToStringTaintFilter.accept(
										editableUseStatement, viewUseStatement)) {
									// we now find toString
									// DbgUtil.printReturnInstanceKey(editableUseStatement,
									// data.dma);
									Statement returnStatement = Helper
											.getReturnStatementForCall(editableUseStatement);
									_seedInstanceKeysMapping.put(
											returnStatement,
											Helper.getReturnInstanceKeys(
													editableUseStatement,
													data.dma));
									_viewSeeds
											.add(Helper
													.getReturnStatementForCall(editableUseStatement));
								}
							}

						}
					}
				}

			}
		}
	}

	@Override
	public String getName() {
		return "PasswordAnalyasisPlugin";
	}

	@Override
	protected void doInitSeeds(AppContext data) {
		doInitViewIds(data);
		doFoundString(data);
	}

}
