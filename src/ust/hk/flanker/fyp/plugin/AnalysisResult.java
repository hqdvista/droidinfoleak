package ust.hk.flanker.fyp.plugin;

public enum AnalysisResult {
	NEGATIVE(0),
	POSITIVE(1),
	CONFIRM(2);
	int result;
	AnalysisResult(int i)
	{
		result = i;
	}
}
