package ust.hk.flanker.fyp.plugin;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Slicer;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.intset.IntSet;

import ust.hk.flanker.fyp.AppContext;
import ust.hk.flanker.fyp.Helper;
import ust.hk.flanker.fyp.filter.PhoneInfoSeedFilter;
import ust.hk.flanker.fyp.filter.GeneralResultFilter;
import ust.hk.flanker.fyp.filter.StringToBytesTaintFilter;
import ust.hk.flanker.fyp.filter.WriteNonPrivateTaintFilter;

public class PhoneInfoAnalysisPlugin extends AnalysisPlugin {
	private static Logger logger = LogManager.getLogger(PhoneInfoAnalysisPlugin.class);
	
	private Queue<Statement> _seedStatements = new LinkedList<Statement>();

	@Override
	public String getName() {
		return "PhoneInfoAnalysisPlugin";
	}

	@Override
	protected void doInitSeeds(AppContext data) {
		PhoneInfoSeedFilter seedfilter = new PhoneInfoSeedFilter();
		for (Iterator<? extends CGNode> it = data._cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			IR ir = n.getIR();
			if (ir == null) {
				continue;
			}
			for (Iterator<SSAInstruction> sit = ir.iterateAllInstructions(); sit
					.hasNext();) {
				SSAInstruction s = sit.next();
				if (seedfilter.accept(n, s)) {
					// found getDeviceId
					IntSet indices = ir
							.getCallInstructionIndices(((SSAInvokeInstruction) s)
									.getCallSite());
					Assertions.productionAssertion(indices.size() == 1,
							"expected 1 but got " + indices.size());
					Statement idReturnStatement = Helper
							.getReturnStatementForCall(new NormalStatement(n,
									indices.intIterator().next()));
					_seedStatements.add(idReturnStatement);
				}
			}
		}
	}

	@Override
	protected void doSlice(AppContext data) {
		GeneralResultFilter generalFilter = new GeneralResultFilter();
		StringToBytesTaintFilter toBytesTaintFilter = new StringToBytesTaintFilter();
		WriteNonPrivateTaintFilter fileFilter = new WriteNonPrivateTaintFilter();
		while (!_seedStatements.isEmpty()) {
			Statement seedStatement = _seedStatements.poll();
			logger.info("seed statement: {}", seedStatement);
			//DbgUtil.contextPrint(statement);
			Collection<Statement> slices = data._slicer.computeForwardThinSlice(seedStatement);
			for (Statement statement : slices) {
				if (generalFilter.accept(statement)) {
					//add tobytes
					if (toBytesTaintFilter.accept(statement, seedStatement)) {
						_seedStatements.add(Helper.getReturnStatementForCall(statement));
					}
					else {
						if (fileFilter.accept(statement, seedStatement)) {
							setResult(AnalysisResult.POSITIVE);
							_resultMapping.put(AnalysisResult.POSITIVE,
									Pair.make(seedStatement, statement));
						}
					}
				}
			}
		}
	}

}
