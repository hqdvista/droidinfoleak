package ust.hk.flanker.fyp.plugin;

import java.util.Set;

import ust.hk.flanker.fyp.AppContext;

import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.collections.HashSetMultiMap;
import com.ibm.wala.util.collections.MultiMap;
import com.ibm.wala.util.collections.Pair;


public abstract class AnalysisPlugin {

	private AnalysisResult _result = AnalysisResult.NEGATIVE;
	public void doAnalysis(AppContext data)
	{
		doInitSeeds(data);
		doSlice(data);
	}
	protected MultiMap<AnalysisResult,Pair<Statement,Statement>> _resultMapping = new HashSetMultiMap<AnalysisResult,Pair<Statement,Statement>>();
	protected void setResult(AnalysisResult result) {
		if (result.result > _result.result) {
			this._result = result;
		}
	}
	
	protected abstract void doInitSeeds(AppContext data);
	protected abstract void doSlice(AppContext data);
	public abstract String getName();
	protected AnalysisResult getResult() {
		return _result;
	}
	
	public String getAnalysisResult()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append(":");
		builder.append("LEVEL:");
		builder.append(getResult());
		builder.append("\n");
		
		for (AnalysisResult result : _resultMapping.keySet()) {
			Set<Pair<Statement, Statement>> statementPairs = _resultMapping.get(result);
			builder.append("RESULT:");
			builder.append(result);
			builder.append("\n");
			builder.append("\n");
			for (Pair<Statement, Statement> pair : statementPairs) {
				builder.append(pair.fst);
				builder.append("\n");
				builder.append(pair.snd);
				builder.append("\n");
			}
			
			builder.append("\n");
		}
		
		return builder.toString();
	}
	
	public String getBriefResult()
	{
		return getName() + ":" + getResult().toString();
	}
	
}
