package ust.hk.flanker.fyp;

import java.util.Collection;

import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.demandpa.util.MemoryAccessMap;
import com.ibm.wala.demandpa.util.PABasedMemoryAccessMap;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.HeapModel;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.intset.OrdinalSet;
import com.ibm.wala.util.strings.Atom;

public class DbgUtil {
	public static void contextPrint(Statement s) {
		if (s.getKind() != Statement.Kind.NORMAL) {
			return;
		}
		NormalStatement normalStatement = ((NormalStatement) s);
		SSAInstruction instruction = normalStatement.getInstruction();
		CGNode node = normalStatement.getNode();
		if (instruction instanceof SSAInvokeInstruction) {
			Atom name = node.getMethod().getDeclaringClass().getClassLoader()
					.getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
				SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
				System.out.println("instruction: " + invoke);
				System.out.println("---");
				System.out.println("method: " + node.getMethod());
				System.out.println("class: "
						+ node.getMethod().getDeclaringClass());
				SSAInstruction arr[] = node.getIR().getInstructions();
				System.out.println("context begin: ");
				for (SSAInstruction ssaInstruction : arr) {
					System.out.println(ssaInstruction);
				}
				System.out.println("context end");
				System.out.println("---");
				System.out.println("---");
			}
		}
	}

	public static void printReturnInstanceKey(Statement s,
			DemandRefinementPointsTo dma) {
		if (s.getKind() == Statement.Kind.NORMAL) {
			System.out.println("statement: " + s);
			NormalStatement normalStatement = (NormalStatement) s;
			SSAInstruction instruction = normalStatement.getInstruction();
			CGNode node = normalStatement.getNode();
			if (instruction instanceof SSAInvokeInstruction) {
				Atom name = node.getMethod().getDeclaringClass()
						.getClassLoader().getName();
				if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
					SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement
							.getInstruction();
					Collection<InstanceKey> instanceKeys = Helper.getReturnInstanceKeys(node, invokeInstruction, dma);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}
				}
			}
		}
	}
	
	public static void printReturnInstanceKey(Statement s,
			PointerAnalysis dma) {
		HeapModel model = dma.getHeapModel();
		if (s.getKind() == Statement.Kind.NORMAL) {
			System.out.println("statement: " + s);
			NormalStatement normalStatement = (NormalStatement) s;
			SSAInstruction instruction = normalStatement.getInstruction();
			CGNode node = normalStatement.getNode();
			if (instruction instanceof SSAInvokeInstruction) {
				Atom name = node.getMethod().getDeclaringClass()
						.getClassLoader().getName();
				if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
					SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement
							.getInstruction();
					int retval = invokeInstruction.getReturnValue(1);
					PointerKey pointerKey = model.getPointerKeyForLocal(node,
							retval);
					System.out.println(pointerKey);
					OrdinalSet<InstanceKey> instanceKeys = dma
							.getPointsToSet(pointerKey);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}
				}
			}
		}
	}

	public static void printArgInstanceKey(Statement s,
			DemandRefinementPointsTo dma) {
		HeapModel model = dma.getHeapModel();
		if (s.getKind() == Statement.Kind.NORMAL) {
			System.out.println("statement: " + s);
			NormalStatement normalStatement = (NormalStatement) s;
			SSAInstruction instruction = normalStatement.getInstruction();
			CGNode node = normalStatement.getNode();
			if (instruction instanceof SSAInvokeInstruction) {
				Atom name = node.getMethod().getDeclaringClass()
						.getClassLoader().getName();
				if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
					SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement
							.getInstruction();
					int use1 = invokeInstruction.getUse(1);
					
					PointerKey pointerKey = model.getPointerKeyForLocal(node,
							use1);
					System.out.println(pointerKey);
					System.out.println("arg1");
					Collection<InstanceKey> instanceKeys = dma
							.getPointsTo(pointerKey);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}
					
					int use2 = invokeInstruction.getUse(2);
					System.out.println("arg2");
					pointerKey = model.getPointerKeyForLocal(node, use2);
					instanceKeys = dma.getPointsTo(pointerKey);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}

				}
			}
		}
	}
	
	public static void printArgInstanceKey(Statement s,
			PointerAnalysis dma) {
		HeapModel model = dma.getHeapModel();
		if (s.getKind() == Statement.Kind.NORMAL) {
			System.out.println("statement: " + s);
			NormalStatement normalStatement = (NormalStatement) s;
			SSAInstruction instruction = normalStatement.getInstruction();
			CGNode node = normalStatement.getNode();
			if (instruction instanceof SSAInvokeInstruction) {
				Atom name = node.getMethod().getDeclaringClass()
						.getClassLoader().getName();
				if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
					SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement
							.getInstruction();
					int use1 = invokeInstruction.getUse(1);
					
					PointerKey pointerKey = model.getPointerKeyForLocal(node,
							use1);
					System.out.println(pointerKey);
					System.out.println("arg1");
					OrdinalSet<InstanceKey> instanceKeys = dma
							.getPointsToSet(pointerKey);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}
					
					int use2 = invokeInstruction.getUse(2);
					System.out.println("arg2");
					pointerKey = model.getPointerKeyForLocal(node, use2);
					instanceKeys = dma.getPointsToSet(pointerKey);
					for (InstanceKey instanceKey : instanceKeys) {
						System.out.println(instanceKey);
					}

				}
			}
		}
	}

}
