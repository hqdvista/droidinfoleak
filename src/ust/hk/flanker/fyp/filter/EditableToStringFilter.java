package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.strings.Atom;

public class EditableToStringFilter extends TaintFilter {

	@Override
	public boolean accept(Statement s, Statement source) {
		if (s.getKind() == Statement.Kind.NORMAL) {
			CGNode node = s.getNode();
			Atom name = node.getMethod().getDeclaringClass()
					.getClassLoader().getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {

				NormalStatement normalStatement = ((NormalStatement) s);
				SSAInstruction instruction = normalStatement
						.getInstruction();
				if (instruction instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
					System.out.println("instruction: "+invoke);
					System.out.println("---");
					System.out.println("method: "+node.getMethod());
					System.out.println("class: "+node.getMethod().getDeclaringClass());
					SSAInstruction arr[] = node.getIR().getInstructions();
					System.out.println("context begin: ");
					for (SSAInstruction ssaInstruction : arr) {
						System.out.println(ssaInstruction);
					}
					System.out.println("context end");
					System.out.println("---");
					System.out.println("---");
					if (invoke
							.getCallSite()
							.getDeclaredTarget()
							.getSignature()
							.equals("android.text.Editable.toString()Ljava/lang/String;")) {
						System.out.println(invoke.getCallSite()
								.getDeclaredTarget().getSignature());
						// found getText
						return true;
					}
				}
			}
		}
		return false;
	}

}
