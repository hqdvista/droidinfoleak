/**
 * 
 */
package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.slicer.Statement;

/**
 * @author hqdvista
 *
 */
public abstract class TaintFilter {
	protected final int id = 0; 
	public abstract boolean accept(Statement slice, Statement source);
	@Override
	public boolean equals(Object arg0) {
		if (arg0 instanceof TaintFilter) {
			if (((TaintFilter) arg0).id == this.id) {
				return true;
			}
		}
		
		return false;
	}
	
	
}
