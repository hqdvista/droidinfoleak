package ust.hk.flanker.fyp.filter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.strings.Atom;

public class GeneralResultFilter {
	private static Logger logger = LogManager.getLogger(GeneralResultFilter.class);

	public boolean accept(Statement s) {
		CGNode node = s.getNode();
		Atom name = node.getMethod().getDeclaringClass().getClassLoader()
				.getName();
		if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))
				&& s.getKind() == Statement.Kind.NORMAL) {
			NormalStatement normalStatement = ((NormalStatement) s);
			SSAInstruction instruction = normalStatement.getInstruction();
			if (instruction instanceof SSAInvokeInstruction) {
				SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
				// filter out e.geterrormsg followed by log.e
				// simple naive method, defuse
				//another way is to do a cross-slice, maybe
				if (invoke.getCallSite().getDeclaredTarget().getSignature()
						.contains("android.util.Log")) {
					SSAInstruction arr[] = node.getIR().getInstructions();
					/* System.out.println("instruction: "+invoke);
					 System.out.println("---");
					 System.out.println("method: "+node.getMethod());
					 System.out
					 .println("class: "+node.getMethod().getDeclaringClass());
					 System.out.println("invoke use:" + invoke.getUse(0));
					 System.out.println("context begin: ");
					 for (int i = 0; i < arr.length; i++) {
							SSAInstruction ssaInstruction = arr[i];
							System.out.println(ssaInstruction);
					 }
					 System.out.println("context end");
					 System.out.println("---");*/
					int use1 = invoke.getUse(0), use2 = invoke.getUse(1);
					for (int i = 0; i < arr.length; i++) {
						SSAInstruction ssaInstruction = arr[i];
						if (ssaInstruction != null) {
							if (ssaInstruction instanceof SSAInvokeInstruction) {
								SSAInvokeInstruction current = (SSAInvokeInstruction) ssaInstruction;
								if (current
										.getCallSite()
										.getDeclaredTarget()
										.getSignature()
										.contains(
												"java.lang.Exception.getMessage")) {
									int def = current.getDef();
									if (def == use1 || def == use2) {
										logger.info("exception get message flow to android.util.log,skip");
										return false;
									}
								}
							}
						}
					}
					
				}

			}
			return true;
		}

		return false;
	}
}
