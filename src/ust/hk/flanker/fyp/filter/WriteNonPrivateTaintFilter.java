package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.strings.Atom;

public class WriteNonPrivateTaintFilter extends TaintFilter {

	@Override
	public boolean accept(Statement s, Statement statement) {
		if (s.getKind() == Statement.Kind.NORMAL) {
			CGNode node = statement.getNode();
			Atom name = node.getMethod().getDeclaringClass()
					.getClassLoader().getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
				NormalStatement normalStatement = ((NormalStatement) s);
				SSAInstruction instruction = normalStatement
						.getInstruction();
				if (instruction instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
					String sig = invoke
							.getCallSite()
							.getDeclaredTarget()
							.getSignature();
					if (sig.contains("java.io.FileOutputstream.write")) {
						return true;
					}
					else if (sig.contains("java.io.BufferedWriter.write")) {
						return true;
					}
					else if (sig.contains("java.io") && sig.contains(".write")) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
