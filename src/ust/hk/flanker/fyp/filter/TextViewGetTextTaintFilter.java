package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.strings.Atom;

public class TextViewGetTextTaintFilter extends TaintFilter {

	@Override
	public boolean accept(Statement s, Statement statement) {
		if (s.getKind() == Statement.Kind.NORMAL) {
			CGNode node = statement.getNode();
			Atom name = node.getMethod().getDeclaringClass()
					.getClassLoader().getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
				NormalStatement normalStatement = ((NormalStatement) s);
				SSAInstruction instruction = normalStatement
						.getInstruction();
				if (instruction instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
					if (invoke
							.getCallSite()
							.getDeclaredTarget()
							.getSignature()
							.toString()
							.equals("android.widget.EditText.getText()Landroid/text/Editable;")) {
						// found getText
						//logger.info("found getText: " + normalStatement);
						//_seeds.add(getReturnStatementForCall(normalStatement));
						return true;
					}
				}
			}
		}
		return false;
	}


}
