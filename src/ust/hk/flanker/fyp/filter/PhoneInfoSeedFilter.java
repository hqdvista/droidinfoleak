package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SymbolTable;

public class PhoneInfoSeedFilter extends SeedFilter {

	/* just monitor getDeviceId
	 * @see ust.hk.flanker.fyp.filter.SeedFilter#accept(com.ibm.wala.ipa.callgraph.CGNode, com.ibm.wala.ssa.SSAInstruction)
	 */
	@Override
	public boolean accept(CGNode n, SSAInstruction s) {
		if (s instanceof SSAInvokeInstruction) {
			SSAInvokeInstruction call = (SSAInvokeInstruction) s;
			if (call.getCallSite().getDeclaredTarget().getName()
						.toString().equals("getDeviceId")) {
					return true;
			}
		}
		return false;
	}

}
