package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ssa.SSAInstruction;

/**
 * @author hqdvista
 * basically, for a sensitive slice, some data flow may be interrupted and we
 * need taint propagation to resume data flow, e.g. toString, string.append
 * So, apply a taintFilter to a statement to decide if we should start another slice from this statement,
 * i.e. put it into worklist
 */
public abstract class SeedFilter{
	public abstract boolean accept(CGNode s, SSAInstruction instruction);
}
