package ust.hk.flanker.fyp.filter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.DefUse;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.strings.Atom;

/**
 * @author hqdvista
 * This filter accepts toString on android.text.Editable
 */
public class EditTextToStringTaintFilter extends TaintFilter {

	@Override
	public boolean accept(Statement slice, Statement source) {
		if (slice.getKind() == Statement.Kind.NORMAL) {
			CGNode node = slice.getNode();
			Atom name = node.getMethod().getDeclaringClass().getClassLoader()
					.getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
				NormalStatement normalStatement = ((NormalStatement) slice);
				SSAInstruction instruction = normalStatement.getInstruction();
				if (instruction instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction invoke = (SSAInvokeInstruction) instruction;
					String sig = invoke
							.getCallSite()
							.getDeclaredTarget()
							.getSignature();
					if (
						sig	.equals("android.text.Editable.toString()Ljava/lang/String;")) {
						return true;
					}
					if (sig.equals("java.lang.Object.toString()Ljava/lang/String;")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	protected final int id = 0x7f7f;
}
