package ust.hk.flanker.fyp.filter;

import java.util.Set;

import org.apache.log4j.Logger;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SymbolTable;

public class PassViewSeedFilter extends SeedFilter {
	private static Logger logger = Logger.getLogger(PassViewSeedFilter.class);
	Set<Integer> integers;
	public PassViewSeedFilter(Set<Integer> integers) {
		this.integers = integers;
	}
	@Override
	public boolean accept(CGNode n, SSAInstruction s) {
		if (s instanceof SSAInvokeInstruction) {
			SSAInvokeInstruction call = (SSAInvokeInstruction) s;
			IR ir = n.getIR();
			SymbolTable symbolTable = ir.getSymbolTable();
			if (integers != null && integers.size() != 0) {
				if (call.getCallSite().getDeclaredTarget().getName()
						.toString().equals("findViewById")) {
					
					int use = s.getUse(1);
					try {
						int id = symbolTable.getIntValue(use);
						for (int viewId : integers) {
							if (id == viewId) {
								logger.info("found passView findViewById statement");
								return true;
							}
						}
					} catch (Exception e) {
						logger.error("while adding " + call, e);
						return false;
					}

				}
			}
		}
		return false;
	}

}
