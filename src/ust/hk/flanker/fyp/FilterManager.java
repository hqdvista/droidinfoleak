package ust.hk.flanker.fyp;

import java.util.HashSet;
import java.util.Set;

import ust.hk.flanker.fyp.filter.SeedFilter;
import ust.hk.flanker.fyp.filter.TaintFilter;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;

public class FilterManager {

	Set<SeedFilter> _seedFilters = new HashSet<SeedFilter>();
	Set<TaintFilter> _taintFilters = new HashSet<TaintFilter>();
	
	public void addSeedFilter(SeedFilter seedFilter)
	{
		_seedFilters.add(seedFilter);
	}
	
	public void addTaintFilter(TaintFilter filter)
	{
		_taintFilters.add(filter);
	}
	
	public boolean applySeedStatement(CGNode node, SSAInstruction instruction)
	{
		for (SeedFilter seedFilter : _seedFilters) {
			if (seedFilter.accept(node,instruction)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean applyTaintStatement(Statement statement, Statement seed)
	{
		for (TaintFilter taintFilter : _taintFilters) {
			if (taintFilter.accept(statement,seed)) {
				return true;
			}
		}
		return false;
	}
}
 