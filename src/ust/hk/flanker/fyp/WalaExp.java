package ust.hk.flanker.fyp;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.propagation.HeapModel;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.util.intset.OrdinalSet;
import com.ibm.wala.util.strings.Atom;

public class WalaExp {

	public static void mai(String args[]) throws IllegalArgumentException,
			ClassHierarchyException, CallGraphBuilderCancelException,
			IOException, InvalidClassFileException {
		LeakAnalysis analysis = new LeakAnalysis(
				"/Users/hqdvista/Documents/hklab/android/casestudy/androidtest/bin");
		analysis.prepare();

		HeapModel model = analysis.get_analysis().getHeapModel();
		CallGraph cg = analysis.get_cg();
		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			IR ir = n.getIR();
			if (ir == null) {
				continue;
			}
			for (Iterator<SSAInstruction> sit = ir.iterateAllInstructions(); sit
					.hasNext();) {

				SSAInstruction s = sit.next();
				// process findViewById related
				if (s instanceof SSAInvokeInstruction) {
					SSAInvokeInstruction invoke = (SSAInvokeInstruction)s;
					Atom name = n.getMethod().getDeclaringClass()
							.getClassLoader().getName();
					if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
						if (s.toString().contains("toString")) {
							System.out.println(s);
							int retval = invoke.getReturnValue(0);
							PointerKey key = model.getPointerKeyForLocal(n, retval);
							OrdinalSet<InstanceKey> instances = analysis.get_analysis().getPointsToSet(key);
							for (InstanceKey instanceKey : instances) {
								System.out.println(instanceKey.toString());
								instanceKey.equals(instanceKey);
							}
						}
					}
				}
			}
		}

	}
	
	public static void main(String args[]) throws InterruptedException {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		for (int i = 0; i < 1; i++) {
			Thread thread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					for(int i=0;i<1000000;i++)
					{
						System.out.println(i+Thread.currentThread().getName());
					}
				}
			});
			thread.start();
			thread.join();
			thread.interrupt();
			/*
			try {
				executor.submit(new Runnable() {
					
					@Override
					public void run() {
						
						try {
							Thread.currentThread().sleep(10000);
						} catch (InterruptedException e) {
							System.out.println("I'm interrupted");
							e.printStackTrace();
						}
						System.out.println("I'm alive again" + Thread.currentThread().getName());
					}
				}).get(1, TimeUnit.SECONDS);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			finally
			{
				
				executor.shutdownNow();
			}
		}
		*/
		}
	}
}
