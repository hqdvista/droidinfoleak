package ust.hk.flanker.fyp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ust.hk.flanker.fyp.plugin.AnalysisPlugin;
import ust.hk.flanker.fyp.plugin.PasswordAnalysisPlugin;
import ust.hk.flanker.fyp.plugin.PhoneInfoAnalysisPlugin;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.demandpa.alg.statemachine.DummyStateMachine;
import com.ibm.wala.demandpa.flowgraph.IFlowLabel;
import com.ibm.wala.demandpa.util.MemoryAccessMap;
import com.ibm.wala.demandpa.util.PABasedMemoryAccessMap;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisOptions.ReflectionOptions;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.ControlFlowContextSelector;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.MyUtil;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.cha.ClassHierarchyException;
import com.ibm.wala.ipa.modref.ModRef;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;
import com.ibm.wala.ipa.slicer.thin.CISlicer;
import com.ibm.wala.ipa.summaries.AndroidMethodTargetSelector;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.strings.Atom;

/**
 * @author hqdvista
 * 
 */
public class LeakAnalysis {
	private static Logger logger = LogManager.getLogger(LeakAnalysis.class);
	private String _root;
	private String _androidPath;
	private List<String> _targetClasses;
	private String _appXMLPath;
	private String _apk;
	private AppContext data = new AppContext(new ArrayList<Entrypoint>());

	/**
	 * @param args
	 * @throws InvalidClassFileException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws CancelException
	 * @throws WalaException
	 */
	public static void main(String[] args) throws IllegalArgumentException,
			IOException, InvalidClassFileException, CancelException,
			WalaException {
		String path = "/Users/hqdvista/Documents/hklab/android/casestudy/androidtest/bin/MainActivity.apk";
		//String path = "/Users/hqdvista/Documents/hklab/android/apps/shuo_android_a23-1.apk";
		//String path = "/Users/hqdvista/Downloads/tuoci.apk";
			if (args.length >= 1) {
				path = args[0];
			}
			LeakAnalysis analysis = new LeakAnalysis(path);
			analysis.prepare();
			System.out.println(analysis.doAnalysis());
	}

	public String doAnalysis() {
		AnalysisPlugin plugin = new PasswordAnalysisPlugin();
		plugin.doAnalysis(data);
		AnalysisPlugin infoPlugin = new PhoneInfoAnalysisPlugin();
		infoPlugin.doAnalysis(data);
		logger.info("done");
		return plugin.getBriefResult() + ", "+infoPlugin.getBriefResult();
	}

	private boolean unpackAPK() throws IOException {
		Properties properties = new Properties();
		System.out.println(new File("config/tool.properties").getAbsolutePath());
		properties.load(new FileInputStream("config/tool.properties"));

		// use apktool to unpack apk
		String filename = new File(_apk).getName();
		data.unpackedPath = "./out/" + filename + "/";
		String[] apktoolCmd = { properties.getProperty("apktool_path"), "d",
				"-f", _apk, data.unpackedPath };
		runTool(apktoolCmd);

		// now res should be at data.unpackedpath

		//workaround for windows bat
		if (properties.getProperty("dex2jar_cmd") == null || properties.getProperty("dex2jar_cmd").equals("")) {
			String[] dex2jarCmd1 = {
					properties.getProperty("dex2jar_file"), _apk, "-f",
					"-o", data.unpackedPath + "app.jar" };
			runTool(dex2jarCmd1);
		}
		else {
			String[] dex2jarCmd2 = { properties.getProperty("dex2jar_cmd"),
					properties.getProperty("dex2jar_file"), _apk, "-f",
					"-o", data.unpackedPath + "app.jar" };
			runTool(dex2jarCmd2);
		}
		
		_androidPath = properties.getProperty("android_platformjar");
		System.out.println(properties.keySet());
		return true;
	}

	private void runTool(String[] apktoolCmd) throws IOException {
		ProcessBuilder builder = new ProcessBuilder(apktoolCmd);
		builder.redirectErrorStream(true);
		Process process = builder.start();

		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String line;
		System.out.printf("Output of running %s is:\n",
				Arrays.toString(apktoolCmd));
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}

		// Wait to get exit value
		try {
			int exitValue = process.waitFor();
			System.out.println("\n\nExit Value is " + exitValue);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public LeakAnalysis(String apk) {
		_apk = apk;
	}

	public void prepare() throws IOException, IllegalArgumentException,
			InvalidClassFileException, ClassHierarchyException,
			CallGraphBuilderCancelException {
		unpackAPK();
		data._scope = AnalysisScopeReader.makePrimordialScope(new File(
				"data/excludes.txt"));
		File rootFile = new File(data.unpackedPath);
		processRoot(rootFile);
		System.out.println(_androidPath);
		data._scope.addToScope(ClassLoaderReference.Primordial, new JarFile(
				_androidPath));

		data._cha = ClassHierarchy.make(data._scope);
		addEntryPoints();

		AnalysisOptions options = new AnalysisOptions(data._scope,
				data._entrypoints);
		options.setReflectionOptions(ReflectionOptions.ONE_FLOW_TO_CAST_ONLY);
		com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = MyUtil
				.makeCustomZeroOneContainerCFABuilder(options,
						new AnalysisCache(), data._cha, data._scope,
						new ControlFlowContextSelector(), null);
		data._androidSelector = (AndroidMethodTargetSelector) options
				.getMethodTargetSelector();
		data._cg = builder.makeCallGraph(options, null);
		data._analysis = builder.getPointerAnalysis();

		data._sdg = new SDG(data._cg, data._analysis,
				DataDependenceOptions.NO_BASE_NO_HEAP_NO_EXCEPTIONS,
				ControlDependenceOptions.NONE);
		data._slicer = new CISlicer(data._sdg, data._analysis, ModRef.make());

		MemoryAccessMap mam = new PABasedMemoryAccessMap(data._cg,
				data._analysis);
		data.dma = DemandRefinementPointsTo.makeWithDefaultFlowGraph(data._cg,
				data._analysis.getHeapModel(), mam, data._cha, options,
				new DummyStateMachine.Factory<IFlowLabel>());
	}

	private static IMethod getMethodFromString(String name, String descriptor,
			IClass iClass) {
		/*
		 * (Landroid/os/Bundle;)V onCreate
		 */
		for (IMethod method : iClass.getAllMethods()) {
			if (method.getDescriptor().toUnicodeString().equals(descriptor)
					&& method.getName().equals(
							Atom.findOrCreateUnicodeAtom(name))
					&& method.getDeclaringClass().getName().getClassName()
							.equals(iClass.getName().getClassName())) {
				return method;
			}
		}
		return null;
	}

	public void addEntryPoints() throws IllegalArgumentException,
			InvalidClassFileException {
		{
			// no entry point specified, auto add
			Iterator<IClass> iterator = data._cha.getLoader(
					ClassLoaderReference.Application).iterateAllClasses();
			while (iterator.hasNext()) {
				IClass iClass = (IClass) iterator.next();
				logger.debug("loaded class:{}", iClass);
				if (!iClass.toString().contains("Login")) {
					// continue;
				}
				// stub: onCreate
				IMethod method = null;
				if ((method = getMethodFromString("onCreate", "",
				// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}
				if ((method = getMethodFromString("onCreate",
						"(Landroid/os/Bundle;)V",
						// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}
				if ((method = getMethodFromString("onPause", "()V",
				// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}

				if ((method = getMethodFromString("onClick",
						"(Landroid/view/View;)V",
						// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}

				if ((method = getMethodFromString("onResume", "()V",
				// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}

				if ((method = getMethodFromString("onDestroy", "()V",
				// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}
				if ((method = getMethodFromString("onRestart", "()V",
				// getMethodFromString("ActivityModel", "()V",
						iClass)) != null) {

					data._entrypoints.add(new DefaultEntrypoint(method,
							data._cha));
				}
				/*
				 * if ((method = getMethodFromString("ActivityModel", "()V",
				 * iClass)) != null) {
				 * 
				 * _entrypoints.add(new DefaultEntrypoint(method, _cha)); }
				 */
			}
		}
	}

	/**
	 * add all class file under dirFile recursively, or add directly if the
	 * dirFile is actually a file
	 * 
	 * @param rootFile
	 * @throws InvalidClassFileException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	private void processRoot(File rootFile) throws IllegalArgumentException,
			InvalidClassFileException, IOException {
		if (!rootFile.isDirectory()) {
			if (rootFile.getName().endsWith(".class")) {
				data._scope.addClassFileToScope(
						ClassLoaderReference.Application, rootFile);
			} else if (rootFile.getName().endsWith(".jar")) {
				data._scope.addToScope(ClassLoaderReference.Application,
						new JarFile(rootFile));
			}
		} else {
			for (File subFile : rootFile.listFiles()) {
				processRoot(subFile);
			}
		}

	}

	protected CallGraph get_cg() {
		return data._cg;
	}

	protected void set_cg(CallGraph _cg) {
		this.data._cg = _cg;
	}

	public PointerAnalysis get_analysis() {
		return data._analysis;
	}

	public void set_analysis(PointerAnalysis _analysis) {
		this.data._analysis = _analysis;
	}
}
