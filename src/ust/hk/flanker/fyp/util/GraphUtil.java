package ust.hk.flanker.fyp.util;

import java.util.Iterator;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.types.Descriptor;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.strings.Atom;

public class GraphUtil {

	  private static CGNode findMethod(CallGraph cg, Descriptor d, Atom name) {
		    for (Iterator<? extends CGNode> it = cg.getSuccNodes(cg.getFakeRootNode()); it.hasNext();) {
		      CGNode n = it.next();
		      if (n.getMethod().getName().equals(name) && n.getMethod().getDescriptor().equals(d)) {
		        return n;
		      }
		    }
		    // if it's not a successor of fake root, just iterate over everything
		    for (CGNode n : cg) {
		      if (n.getMethod().getName().equals(name) && n.getMethod().getDescriptor().equals(d)) {
		        return n;
		      }
		    }
		    Assertions.UNREACHABLE("failed to find method " + name);
		    return null;
		  }

		  public static CGNode findMethod(CallGraph cg, String name) {
		    Atom a = Atom.findOrCreateUnicodeAtom(name);
		    for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
		      CGNode n = it.next();
		      if (n.getMethod().getName().equals(a)) {
		        return n;
		      }
		    }
		    System.err.println("call graph " + cg);
		    Assertions.UNREACHABLE("failed to find method " + name);
		    return null;
		  }

		  public static Statement findCallTo(CGNode n, String methodName) {
		    IR ir = n.getIR();
		    for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
		      SSAInstruction s = it.next();
		      if (s instanceof SSAInvokeInstruction) {
		        SSAInvokeInstruction call = (SSAInvokeInstruction) s;
		        if (call.getCallSite().getDeclaredTarget().getName().toString().equals(methodName)) {
		          IntSet indices = ir.getCallInstructionIndices(((SSAInvokeInstruction) s).getCallSite());
		          Assertions.productionAssertion(indices.size() == 1, "expected 1 but got " + indices.size());
		          return new NormalStatement(n, indices.intIterator().next());
		        }
		      }
		    }
		    Assertions.UNREACHABLE("failed to find call to " + methodName + " in " + n);
		    return null;
		  }

		  public static Statement findFirstAllocation(CGNode n) {
		    IR ir = n.getIR();
		    for (int i = 0; i < ir.getInstructions().length; i++) {
		      SSAInstruction s = ir.getInstructions()[i];
		      if (s instanceof SSANewInstruction) {
		        return new NormalStatement(n, i);
		      }
		    }
		    Assertions.UNREACHABLE("failed to find allocation in " + n);
		    return null;
		  }

}
