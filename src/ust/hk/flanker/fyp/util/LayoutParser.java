package ust.hk.flanker.fyp.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.ShrikeClass;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.shrikeCT.ClassReader;
import com.ibm.wala.shrikeCT.ConstantPoolParser;
import com.ibm.wala.shrikeCT.ConstantValueReader;
import com.ibm.wala.shrikeCT.ClassReader.AttrIterator;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.collections.Pair;

public class LayoutParser {
	private static Logger logger = LogManager.getLogger(LayoutParser.class);
	private static final String R = "R";
	private static final String RS = "R$";
	private static Pattern pattern = Pattern.compile("\\W*(\\w*)/(\\w*)");
	private static final Namespace androidSpace = Namespace
			.getNamespace("http://schemas.android.com/apk/res/android");

	/**
	 * @param file path of resource file, e.g. /Users/hqdvista/androidtools/shuo/res
	 * @param hierarchy
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws InvalidClassFileException
	 */
	static public Set<Integer> getPasswordInputIds(File file)
			throws JDOMException, IOException, IllegalArgumentException,
			InvalidClassFileException {
		List<String> passIds = parseXML(file);
		logger.info("passids: "+passIds);
		Set<Integer> ret = new HashSet<Integer>();
		List<Pair<String, String>> idInfos = new ArrayList<Pair<String, String>>();
		// id is in the format of @+id/editText1
		// extract {id:editText1} Pair
		for (String string : passIds) {
			Matcher matcher = pattern.matcher(string);
			if (matcher.find() && matcher.groupCount() == 2) {
				Pair<String, String> id = Pair.make(matcher.group(1),
						matcher.group(2));
				idInfos.add(id);
			} else {
				logger.error("found android id not match regex: " + string);
			}

		}

		
		String publicXMLpath = file.getAbsolutePath() + "/values/public.xml";
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(publicXMLpath);
		Element root = doc.getRootElement();
		for (Element element : root.getChildren()) {
			for (Pair<String, String> pair : idInfos) {
				if (element.getAttributeValue("type").equals(pair.fst) && element.getAttributeValue("name").equals(pair.snd)) {
					ret.add(Integer.parseInt(element.getAttributeValue("id").substring(2),16));
				}
			}
		}
		// parse all R$*.java
		/*
		Iterator<IClass> iterator = hierarchy.getLoader(
				ClassLoaderReference.Application).iterateAllClasses();
		while (iterator.hasNext()) {
			IClass iClass = iterator.next();
			String name = iClass.getName().toString();
			String arr[] = name.split("/");
			String lastname = arr[arr.length - 1];
			for (Pair<String, String> pair : idInfos) {
				
				if (lastname.equals(RS + pair.fst)) {
					ClassReader reader = ((ShrikeClass) iClass).getReader();
					ConstantPoolParser parser = reader.getCP();
					AttrIterator iter = new AttrIterator();
					int fieldCount = reader.getFieldCount();
					for (int i = 0; i < fieldCount; i++) {
						if (reader.getFieldName(i).equals(pair.snd)) {
							reader.initFieldAttributeIterator(i, iter);
							for (; iter.isValid(); iter.advance()) {
								if (iter.getName().equals("ConstantValue")) {
									ConstantValueReader cv = new ConstantValueReader(
											iter);
									if (parser
											.getItemType(cv.getValueCPIndex()) == ConstantPoolParser.CONSTANT_Integer) {
										ret.add(parser.getCPInt(cv
												.getValueCPIndex()));
									}
									break;
								}
							}
							
						}

					}
				}
			}

		}
	*/
		if (ret.size() != idInfos.size()) {
			logger.debug("some layout ids failed to be found in R");
		}
		
		//ret.add(16908309); debug for douban-shuo
		logger.info(ret);
		return ret;
	}

	static private void parseItem(Element element, List<String> ret)
	{
		Attribute attribute = element.getAttribute("inputType",
				androidSpace);
		if (attribute != null
				&& attribute.getValue().equals("textPassword")) {
			// found password input
			ret.add(element.getAttributeValue("id", androidSpace));
		}
		else {
			//android:password=true
			attribute = element.getAttribute("password", androidSpace);
			if (attribute != null && attribute.getValue().equals("true")) {
				ret.add(element.getAttributeValue("id", androidSpace));
			}
		}
		
		for (Element element2 : element.getChildren()) {
			parseItem(element2, ret);
		}
	}
	static private List<String> parseXML(File xmlFile) throws JDOMException, IOException {
		List<String> ret = new ArrayList<String>();
		if (xmlFile.isDirectory()) {
			for (File subFile : xmlFile.listFiles()) {
				ret.addAll(parseXML(subFile));
			}
		} else {
			if (xmlFile.getName().endsWith(".xml")) {
				SAXBuilder builder = new SAXBuilder();
				Document doc = builder.build(xmlFile);
				Element root = doc.getRootElement();
				parseItem(root, ret);
			}
		}
		return ret;
	}

	public static void main(String args[]) {
		try {
			new LayoutParser()
					.getPasswordInputIds(new File(
							"/Users/hqdvista/androidtools/towords/res"));
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidClassFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
