package ust.hk.flanker.fyp.util;

import java.math.BigInteger;
import java.util.Iterator;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;

public class DebugUtil {

	public static void printOneLevelCGFromEntryNode(CallGraph cg) {
		Iterator<CGNode> iterator = cg.getEntrypointNodes().iterator();
	      while (iterator.hasNext()) {
			CGNode cgNode = (CGNode) iterator.next();
			Iterator<CGNode> succnodeiter = cg.getSuccNodes(cgNode);
			System.out.println(cgNode);
			
			while (succnodeiter.hasNext()) {
				CGNode succnode = (CGNode) succnodeiter.next();
				System.out.println(succnode);
			}
			break;
		}
	}

}
