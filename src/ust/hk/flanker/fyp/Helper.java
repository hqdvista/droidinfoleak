package ust.hk.flanker.fyp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.ibm.wala.classLoader.ShrikeBTMethod;
import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.examples.properties.WalaExamplesProperties;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.slicer.HeapStatement;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.properties.WalaProperties;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.CancelException;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.collections.Filter;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.GraphSlicer;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.strings.Atom;
import com.ibm.wala.viz.NodeDecorator;
import com.ibm.wala.viz.PDFViewUtil;

public class Helper {
	/**
	 * If s is a call statement, return the statement representing the normal
	 * return from s
	 */
	public static Statement getReturnStatementForCall(Statement s) {
		if (s.getKind() == Kind.NORMAL) {
			NormalStatement n = (NormalStatement) s;
			SSAInstruction st = n.getInstruction();
			if (st instanceof SSAInvokeInstruction) {
				SSAAbstractInvokeInstruction call = (SSAAbstractInvokeInstruction) st;
				if (call.getCallSite().getDeclaredTarget().getReturnType()
						.equals(TypeReference.Void)) {
					throw new IllegalArgumentException(
							"this driver computes forward slices from the return value of calls.\n"
									+ ""
									+ "Method "
									+ call.getCallSite().getDeclaredTarget()
											.getSignature() + " returns void.");
				}
				return new NormalReturnCaller(s.getNode(),
						n.getInstructionIndex());
			} else {
				return s;
			}
		} else {
			return s;
		}
	}
	
	public static Collection<InstanceKey> getReturnInstanceKeys(CGNode node,SSAInvokeInstruction invokeInstruction, DemandRefinementPointsTo dma)
	{
		int retval = invokeInstruction.getReturnValue(1);
		PointerKey pointerKey = dma.getHeapModel().getPointerKeyForLocal(node,
				retval);
		Collection<InstanceKey> instanceKeys = dma
				.getPointsTo(pointerKey);
		return instanceKeys;
	}
	
	public static Collection<InstanceKey> getReturnInstanceKeys(Statement statement, DemandRefinementPointsTo dma)
	{
		NormalStatement normalStatement = (NormalStatement)statement;
		CGNode node = statement.getNode();
		SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement.getInstruction();
		int retval = invokeInstruction.getReturnValue(1);
		PointerKey pointerKey = dma.getHeapModel().getPointerKeyForLocal(node,
				retval);
		Collection<InstanceKey> instanceKeys = dma
				.getPointsTo(pointerKey);
		return instanceKeys;
	}
	
	public static Collection<InstanceKey> getArgInstanceKeys(Statement statement, DemandRefinementPointsTo dma,int pos)
	{
		NormalStatement normalStatement = (NormalStatement)statement;
		CGNode node = statement.getNode();
		SSAInvokeInstruction invokeInstruction = (SSAInvokeInstruction) normalStatement.getInstruction();
		int use1 = invokeInstruction.getUse(pos);
		
		PointerKey pointerKey = dma.getHeapModel().getPointerKeyForLocal(node,
				use1);
		System.out.println(pointerKey);
		System.out.println("arg1");
		Collection<InstanceKey> instanceKeys = dma
				.getPointsTo(pointerKey);
		return instanceKeys;
	}
	
	public static Statement findCallTo(CGNode n, String methodName) {
		IR ir = n.getIR();
		for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it
				.hasNext();) {
			SSAInstruction s = it.next();
			if (s instanceof SSAInvokeInstruction) {
				SSAInvokeInstruction call = (SSAInvokeInstruction) s;

				if (call.getCallSite().getDeclaredTarget().getName().toString()
						.equals(methodName)) {
					IntSet indices = ir
							.getCallInstructionIndices(((SSAInvokeInstruction) s)
									.getCallSite());
					Assertions.productionAssertion(indices.size() == 1,
							"expected 1 but got " + indices.size());
					return new NormalStatement(n, indices.intIterator().next());
				}
			}
		}
		Assertions.UNREACHABLE("failed to find call to " + methodName + " in "
				+ n);
		return null;
	}

	public static Statement findFirstAllocation(CGNode n) {
		IR ir = n.getIR();
		for (int i = 0; i < ir.getInstructions().length; i++) {
			SSAInstruction s = ir.getInstructions()[i];
			if (s instanceof SSANewInstruction) {
				return new NormalStatement(n, i);
			}
		}
		Assertions.UNREACHABLE("failed to find allocation in " + n);
		return null;
	}

	public static CGNode findMethod(CallGraph cg, String name, String className) {
		Atom a = Atom.findOrCreateUnicodeAtom(name);
		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			System.out.println(n.getMethod().getName());
			System.out.println(n.getMethod().getDeclaringClass().getName());
			if (n.getMethod().getName().equals(a)
					&& n.getMethod().getDeclaringClass().getName().toString()
							.equals(className)) {
				System.out.println(n.getMethod().getDeclaringClass().getName());
				System.out.println(n.getMethod().getDeclaringClass().getName()
						.equals(className));
				System.out.println(n.getMethod().getDeclaringClass().getName()
						.equals(Atom.findOrCreateUnicodeAtom(className)));
				return n;
			}
		}
		// System.err.println("call graph " + cg);
		Assertions.UNREACHABLE("failed to find method " + name);
		return null;
	}

	

	/**
	 * return a view of the sdg restricted to the statements in the slice
	 */
	public static Graph<Statement> pruneSDG(SDG sdg,
			final Collection<Statement> slice) {
		Filter<Statement> f = new Filter<Statement>() {
			public boolean accepts(Statement o) {
				return slice.contains(o);
			}
		};
		return GraphSlicer.prune(sdg, f);
	}

	/**
	 * @return a NodeDecorator that decorates statements in a slice for a
	 *         dot-ted representation
	 */
	public static NodeDecorator makeNodeDecorator() {
		return new NodeDecorator() {
			public String getLabel(Object o) throws WalaException {
				Statement s = (Statement) o;
				switch (s.getKind()) {
				case HEAP_PARAM_CALLEE:
				case HEAP_PARAM_CALLER:
				case HEAP_RET_CALLEE:
				case HEAP_RET_CALLER:
					HeapStatement h = (HeapStatement) s;
					return s.getKind() + "\\n" + h.getNode() + "\\n"
							+ h.getLocation();
				case NORMAL:
					NormalStatement n = (NormalStatement) s;
					return n.getInstruction() + "\\n"
							+ n.getNode().getMethod().getSignature();
				case PARAM_CALLEE:
					ParamCallee paramCallee = (ParamCallee) s;
					return s.getKind() + " " + paramCallee.getValueNumber()
							+ "\\n" + s.getNode().getMethod().getName();
				case PARAM_CALLER:
					ParamCaller paramCaller = (ParamCaller) s;
					return s.getKind()
							+ " "
							+ paramCaller.getValueNumber()
							+ "\\n"
							+ s.getNode().getMethod().getName()
							+ "\\n"
							+ paramCaller.getInstruction().getCallSite()
									.getDeclaredTarget().getName();
				case EXC_RET_CALLEE:
				case EXC_RET_CALLER:
				case NORMAL_RET_CALLEE:
				case NORMAL_RET_CALLER:
				case PHI:
				default:
					return s.toString();
				}
			}

		};
	}
	
	/*
	public static Collection<Statement> getForwardSlices(String caller, String callee,
			String callerClass, CallGraph cg, PointerAnalysis analysis)
			throws IllegalArgumentException, CancelException, WalaException {

		CGNode callerNode = LeakAnalysis.findMethod(cg, caller, callerClass);
		Statement s = LeakAnalysis.findCallTo(callerNode, callee);
		logger.debug("seed statement: {}", s);
		s = Helper.getReturnStatementForCall(s);

		Collection<Statement> slice = data._slicer.computeForwardThinSlice(s);
		// Collection<Statement> slice = Slicer.computeForwardSlice(sdg, s);
		// create a view of the SDG restricted to nodes in the slice

		Graph<Statement> g = pruneSDG(data._sdg, slice);

		// load Properties from standard WALA and the WALA examples project
		Properties p = null;
		try {
			p = WalaExamplesProperties.loadProperties();
			p.putAll(WalaProperties.loadProperties());
		} catch (WalaException e) {
			e.printStackTrace();
			Assertions.UNREACHABLE();
		}
		// create a dot representation.
		String psFile = p.getProperty(WalaProperties.OUTPUT_DIR)
				+ File.separatorChar + "slice.pdf";
		String dotExe = p.getProperty(WalaExamplesProperties.DOT_EXE);
		// DotUtil.dotify(g, makeNodeDecorator(), PDFTypeHierarchy.DOT_FILE,
		// psFile, dotExe);

		// fire off the PDF viewer
		String gvExe = p.getProperty(WalaExamplesProperties.PDFVIEW_EXE);
		PDFViewUtil.launchPDFView(psFile, gvExe);
		return slice;
	}
	*/
	

	public List<SSAInstruction> retrieveAppInst(Collection<Statement> slice) {
		List<SSAInstruction> ret = new ArrayList<SSAInstruction>();
		for (Iterator iterator = slice.iterator(); iterator.hasNext();) {
			Statement statement = (Statement) iterator.next();
			CGNode node = statement.getNode();
			Atom name = node.getMethod().getDeclaringClass().getClassLoader()
					.getName();
			if (name.equals(Atom.findOrCreateUnicodeAtom("Application"))) {
				// ignore non-application statements
				if (statement.getKind() == Statement.Kind.NORMAL) {
					NormalStatement normalStatement = ((NormalStatement) statement);
					SSAInstruction instruction = normalStatement
							.getInstruction();
					ret.add(instruction);
					System.out.println(instruction);
					int bcIndex, instructionIndex = normalStatement
							.getInstructionIndex();
					try {
						bcIndex = ((ShrikeBTMethod) normalStatement.getNode()
								.getMethod())
								.getBytecodeIndex(instructionIndex);
						try {
							int src_line_number = normalStatement.getNode()
									.getMethod().getLineNumber(bcIndex);
							System.out.println(normalStatement.getNode()
									.getMethod().getSignature());
							System.err.println("Source line number = "
									+ src_line_number);
						} catch (Exception e) {
							System.err.println("Bytecode index no good");
							System.err.println(e.getMessage());
						}
					} catch (Exception e) {
						System.err
								.println("it's probably not a BT method (e.g. it's a fakeroot method)");
						System.err.println(e.getMessage());
					}
				}
			}
		}
		return ret;
	}

}
