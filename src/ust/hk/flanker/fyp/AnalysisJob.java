package ust.hk.flanker.fyp;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AnalysisJob implements Runnable {
	private static Logger logger = LogManager.getLogger(AnalysisJob.class);
	private File apkFile;

	@Override
	public void run() {
		LeakAnalysis analysis = new LeakAnalysis(
		// "/Users/hqdvista/Documents/laoheshanxia/app/android/laoheshanxia-stable/android/LaoHeShanXia/bin/classes",
		// "/Users/hqdvista/Dropbox/fun/androidev/dec-classes",
		// "C:\\Users\\Administrator\\Dropbox\\hklab\\android\\casestudy\\androidtest\\bin",
		// "C:\\Users\\Administrator\\Documents\\android\\casestudy\\dec-classes",
		// "/Users/hqdvista/Documents/hklab/android/casestudy/androidtest/bin",
		// "/Users/hqdvista/Documents/hklab/android/apps/shuo_android_a23-1-dex2jar.jar",
		// "/Users/hqdvista/androidtools/com.douban.book.reader.1305271100-dex2jar.jar",
				apkFile.getAbsolutePath()
				);
		try {
			analysis.prepare();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		/*
		 * try { SWTCallGraph.runWithCallGraph(analysis.data._cg); } catch
		 * (WalaException e) { e.printStackTrace(); }
		 */
		analysis.doAnalysis();
		// Collection<Statement> slice =
		// analysis.getForwardSlices("onCreate",
		// "toString", "Lcom/example/androidtest/MainActivity",analysis._cg,
		// analysis._analysis);

		// List<SSAInstruction> instructions =
		// analysis.retrieveAppInst(slice);
		// for (SSAInstruction ssaInstruction : instructions) {
		// System.out.println(ssaInstruction);
		// }
		logger.info("slicing done");
	}

	public AnalysisJob(File apkFile) {
		this.apkFile = apkFile;
	}

	public static void main(String []args) {
		File file = new File(
				"/Users/hqdvista/Dropbox/fun/androidev/apks/com.tencent/com.tencent.news.apk");
		ExecutorService executor = Executors.newSingleThreadExecutor();
		//for (File apkFile : file.listFiles()) 
		{
			AnalysisJob job = new AnalysisJob(file);
			
			try {
				executor.submit(job).get(10, TimeUnit.MINUTES);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//executor.shutdown();
			}
			finally
			{
				executor.shutdown();
			}
			
		}
	}
}
