package ust.hk.flanker.fyp;

import java.util.List;

import com.ibm.wala.demandpa.alg.DemandRefinementPointsTo;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.thin.CISlicer;
import com.ibm.wala.ipa.summaries.AndroidMethodTargetSelector;

public class AppContext {
	public AnalysisScope _scope;
	public ClassHierarchy _cha;
	public List<Entrypoint> _entrypoints;
	public AndroidMethodTargetSelector _androidSelector;
	public PointerAnalysis _analysis;
	public CallGraph _cg;
	public SDG _sdg;
	public CISlicer _slicer;
	public DemandRefinementPointsTo dma;

	public AppContext(List<Entrypoint> _entrypoints) {
		this._entrypoints = _entrypoints;
	}
	
	public String unpackedPath;
}