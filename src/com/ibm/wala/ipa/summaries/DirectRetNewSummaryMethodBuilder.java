package com.ibm.wala.ipa.summaries;

import static com.ibm.wala.types.TypeName.ArrayMask;
import static com.ibm.wala.types.TypeName.ElementBits;
import static com.ibm.wala.types.TypeName.PrimitiveMask;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ust.hk.flanker.fyp.LeakAnalysis;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.Language;
import com.ibm.wala.classLoader.NewSiteReference;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ssa.ISSABasicBlock;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInstructionFactory;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.collections.HashMapFactory;
import com.ibm.wala.util.debug.Assertions;

public class DirectRetNewSummaryMethodBuilder extends AbstractMethodBuilder {
	private static Logger logger = LogManager.getLogger(DirectRetNewSummaryMethodBuilder.class);
	private static final String V_NULL = "null";

	private int _nextLocal;
	private HashMap<String, Integer> _symbolTable;
	private TypeReference _actualReturnType;
	
	@Override
	public MethodSummary toMethod(CGNode node, CallSiteReference site)
	{
		super.toMethod(node, site);
		prepareMethodForReturnNew(node, site);
		if (site.toString().contains("findViewById")) {
			_methodSummary.setFactory(true);
		}
		return _methodSummary;
	}
	
	private TypeReference findConcreteDescendant(IClass ancestor)
	{
		//fuck, choose first concrete subtype
        Collection<IClass> targets;
        Queue<IClass> queue = new LinkedList<IClass>();
        queue.add(ancestor);
        final int BOUND = 20;//max resolve count: 20
        int i = 0;
        while(!queue.isEmpty())
        {
        	IClass item = queue.remove();
        	if (item == null) {
				continue;
			}
        	if (!(item.isAbstract() || item.isInterface())) {
				return item.getReference();
			}
        	TypeReference reference = item.getReference();
        	queue.addAll(_cha.computeSubClasses(reference));
        	queue.addAll(_cha.getImplementors(reference));
        	i++;
        	if (i > BOUND) {
				break;
			}
        }
        
        return null;
	}

	/**
	 * we need context information
	 * 
	 * @param node
	 * @param site
	 */
	private void prepareMethodForReturnNew(CGNode node, CallSiteReference site) {
		TypeReference classTypeReference = site.getDeclaredTarget().getDeclaringClass();
		// just return new
		ISSABasicBlock[] blocks = node.getIR().getBasicBlocksForCall(site);
		/*if (blocks.length != 1) {
			logger.error(blocks);
			int i = 0;
			for (ISSABasicBlock issaBasicBlock : blocks) {
				logger.error("block :" + i);
				for (SSAInstruction ssaInstruction : issaBasicBlock) {
					logger.error(ssaInstruction);
				}
				++i;
			}
			throw new IllegalArgumentException(
					"block size isn't one, phi node?");
		}*/
		logger.debug("resolving site: "+site.toString());
		SSAInstruction instruction = blocks[0].iterator().next();
		ISSABasicBlock succBlock = node.getIR().getControlFlowGraph()
				.getSuccNodes(blocks[0]).next();
		TypeReference reference = site.getDeclaredTarget().getReturnType();// default no cast

        IClass retClass = _cha.lookupClass(reference);
		boolean resolved = false;
		logger.debug("reference: " + reference.toString());
		if (reference.isReferenceType() && !reference.isArrayType()) {
			//TODO: fix array type
			logger.debug("succ block: "+succBlock);
			if(!resolved && succBlock.iterator().hasNext())
			{
				SSAInstruction succInstruction = succBlock.iterator().next();
				logger.debug("succ instruction: "+succInstruction.toString());
				if (succInstruction instanceof SSACheckCastInstruction) {
					// immediate checkcast follows
					reference = ((SSACheckCastInstruction) succInstruction)
							.getDeclaredResultType();
					logger.debug("find cast, actual type" + reference);
					resolved = true;
				}
				else {
			      //no checkcast, see if is like java/lang/CharSequence(interface) toText
					  //avoid getContext().getFileOutputStream
					  if (reference.isReferenceType() && retClass != null) {
			        if (retClass.isAbstract() || retClass.isInterface()) {
			          //non-concrete class, try to resolve it by next function call
			          if (succInstruction instanceof SSAInvokeInstruction) {
			            TypeReference instReference = ((SSAInvokeInstruction) succInstruction).getDeclaredResultType();
			            IClass concreteClass = _cha.lookupClass(instReference);
			            if (concreteClass != null && ( _cha.isSubclassOf(concreteClass, retClass) || (retClass.isInterface() && _cha.implementsInterface(concreteClass, retClass)))) {
			              reference = instReference;
			              resolved = true;
			            }
			          }
			          
			        }
			      }
			    }
			}
			

			if (retClass != null && !(retClass.isAbstract() || retClass.isInterface())) {
				resolved = true;
			}
			 //fall back.. still cannot resolve?
	        if (!resolved) {
	        	reference = findConcreteDescendant(retClass);
	        	if (reference == null) {
					logger.error("cannot found concrete impl for class: " + retClass);
				}
	        }
		
		}


		_methodSummary = new MethodSummary(site.getDeclaredTarget());
		if (_realMethod.isStatic()) {
			_methodSummary.setStatic(true);
		}

		int nParams = site.getDeclaredTarget().getNumberOfParameters();
		if (!_realMethod.isStatic()) {
			nParams += 1;
		}

		// note that symbol tables reserve v0 for "unknown", so v1 gets assigned
		// to the first parameter "arg0", and so forth.
		_nextLocal = nParams + 1;
		_symbolTable = HashMapFactory.make(5);
		// create symbols for the parameters
		for (int i = 0; i < nParams; i++) {
			_symbolTable.put("arg" + i, new Integer(i + 1));
		}

		_actualReturnType = reference;
		
		Language lang = _scope.getLanguage(_loadeReference.getLanguage());
		SSAInstructionFactory insts = lang.instructionFactory();

		// deduce the concrete type allocated
		final TypeReference type = _actualReturnType;

		// register the local variable defined by this allocation
		String defVar = "x";
		if (_symbolTable.keySet().contains(defVar)) {
			Assertions.UNREACHABLE("Cannot def variable twice: " + defVar
					+ " in " + _methodSummary);
		}
		/*
		 * if (defVar == null) { // the method summary ignores the def'ed
		 * variable. // just allocate a temporary defVar = "L" + nextLocal; }
		 */
		int defNum = _nextLocal;
		_symbolTable.put(defVar, new Integer(_nextLocal++));
		// create the allocation statement and add it to the method summary
		NewSiteReference ref = NewSiteReference.make(
				_methodSummary.getNextProgramCounter(), type);

		SSANewInstruction a = null;
		if (type.isArrayType()) {
			String size = "arg"+(_nextLocal);
			Assertions.productionAssertion(size != null);
			Integer sNumber = _symbolTable.get(size);
			if (sNumber == null) {
        defNum = _nextLocal;
        _symbolTable.put(size, _nextLocal++);
        sNumber = _symbolTable.get(size);
      }
			Assertions.productionAssertion(sNumber != null);
			Assertions.productionAssertion(
			// array of objects
					type.getDerivedMask() == ArrayMask ||
					// array of primitives
							type.getDerivedMask() == ((ArrayMask << ElementBits) | PrimitiveMask));
			a = insts.NewInstruction(defNum, ref,
					new int[] { sNumber.intValue() });
		} else {
			a = insts.NewInstruction(defNum, ref);
		}
		
		logger.debug("Builder: adding new inst: "+a);
		_methodSummary.addStatement(a);

		if (_methodSummary.getReturnType() != null) {
			String retV = "x";
			Integer valueNumber = _symbolTable.get(retV);
			if (valueNumber == null) {
				if (!retV.equals(V_NULL)) {
					Assertions.UNREACHABLE("Cannot return value with no def: "
							+ retV);
				} else {
					valueNumber = _symbolTable.get(V_NULL);
					if (valueNumber == null) {
						valueNumber = new Integer(_nextLocal++);
						_symbolTable.put(V_NULL, valueNumber);
					}
				}
			}
			boolean isPrimitive = _methodSummary.getReturnType()
					.isPrimitiveType();
			SSAReturnInstruction R = insts.ReturnInstruction(
					valueNumber.intValue(), isPrimitive);
			_methodSummary.addStatement(R);
		}
	}
}


