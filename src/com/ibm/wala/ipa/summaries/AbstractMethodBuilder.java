package com.ibm.wala.ipa.summaries;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.types.ClassLoaderReference;

public abstract class AbstractMethodBuilder {

	/**
	 * @param node caller the CGNode in the call graph containing the call
	 * @param site the call site reference of the call site
	 * @return methodsummary for this context of callsite
	 */
	protected MethodSummary toMethod(CGNode node, CallSiteReference site)
	{
		if (_prepared) {
			throw new IllegalArgumentException("builder already used");
		}
		_prepared = true;
		return null;
	}

	public final void setClassLoader(ClassLoaderReference loaderReference)
	{
		_loadeReference = loaderReference;
	}

	public final void setRealMethod(IMethod realMethod)
	{
		_realMethod = realMethod;
	}

	public final void setcha(IClassHierarchy cha)
	{
		_cha = cha;
	}
	
	public final void setScope(AnalysisScope scope)
	{
		_scope = scope;
	}
	protected AnalysisScope _scope;
	protected ClassLoaderReference _loadeReference;
	protected MethodSummary _methodSummary;
	protected IClassHierarchy _cha;
	protected IMethod _realMethod;
	private boolean _prepared = false;

}