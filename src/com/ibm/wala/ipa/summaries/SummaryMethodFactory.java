package com.ibm.wala.ipa.summaries;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ust.hk.flanker.fyp.LeakAnalysis;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.MethodTargetSelector;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.util.collections.Pair;

public class SummaryMethodFactory {
	private static Logger logger = LogManager.getLogger(SummaryMethodFactory.class);
	private Map<Pair<MethodReference, Integer>, SummarizedMethod> _missingNodes;
	private MethodTargetSelector _parent;
	private IClassHierarchy _cha;
	private AnalysisScope _scope;

	public SummaryMethodFactory(
			Map<Pair<MethodReference, Integer>, SummarizedMethod> _missingNodes) {
		this._missingNodes = _missingNodes;
	}

	public Map<Pair<MethodReference, Integer>, SummarizedMethod> get_missingNodes() {
		return _missingNodes;
	}

	public void set_missingNodes(
			Map<Pair<MethodReference, Integer>, SummarizedMethod> _missingNodes) {
		this._missingNodes = _missingNodes;
	}

	public MethodTargetSelector get_parent() {
		return _parent;
	}

	public void set_parent(MethodTargetSelector _parent) {
		this._parent = _parent;
	}

	public IClassHierarchy get_cha() {
		return _cha;
	}

	public void set_cha(IClassHierarchy _cha) {
		this._cha = _cha;
	}

	public AnalysisScope get_scope() {
		return _scope;
	}

	public void set_scope(AnalysisScope _scope) {
		this._scope = _scope;
	}
	
	/*
	 * A. control flow related
	 * A.1. java.lang.Thread.run
	 * A.2. AsyncTask.start
	 * 
	 * B. android api call
	 * B.1 getTextView
	 */
	private  SummarizedMethod generateMethod(CGNode node,
			CallSiteReference site, IMethod realIMethod, AbstractMethodBuilder builder) {
		builder.setcha(_cha);
		builder.setClassLoader(realIMethod.getDeclaringClass().getClassLoader()
				.getReference());
		builder.setRealMethod(realIMethod);
		builder.setScope(_scope);
		return new SummarizedMethod(site.getDeclaredTarget(),
				builder.toMethod(node, site), _cha.lookupClass(site.getDeclaredTarget()
						.getDeclaringClass()));
	}
	
	public IMethod findOrCreateMethodFromCache(CGNode caller,
			CallSiteReference site, IMethod realMethod,
			Pair<MethodReference, Integer> pair, AbstractMethodBuilder builder) {
		SummarizedMethod summary = null;
		// System.out.println("android system call");
		if ((summary = _missingNodes.get(pair)) != null) {
			logger.debug("in cache");
			return summary;
		} else {
			summary = this.generateMethod(caller, site, realMethod,builder);
			_missingNodes.put(pair, summary);
			logger.debug("generate summary: "+ summary);
			// System.out.println("generate summary: " + summary);
			return summary;
		}
	}
}