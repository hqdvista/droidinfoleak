package com.ibm.wala.ipa.summaries;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.MethodTargetSelector;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.strings.Atom;

public class AndroidMethodTargetSelector implements MethodTargetSelector {
	private static Logger logger = LogManager.getLogger(DirectRetNewSummaryMethodBuilder.class);
	
	private SummaryMethodFactory factory= new SummaryMethodFactory(new HashMap<Pair<MethodReference, Integer>, SummarizedMethod>());

	public AndroidMethodTargetSelector(MethodTargetSelector parent,
			Map<MethodReference, MethodSummary> methodSummaries,
			Set<Atom> ignoredPackages, IClassHierarchy cha, AnalysisScope scope) {
		factory.set_parent(parent);
		factory.set_cha(cha);
		factory.set_scope(scope);
	}

	
	public IMethod getCalleeTarget(CGNode caller, CallSiteReference site,
			IClass dispatchType) {
		// System.out.println("try to resolve site:" + site);
		IMethod realMethod = factory.get_parent().
				getCalleeTarget(caller, site, dispatchType);
		if (realMethod == null) {
			return null;
		}
		Pair<MethodReference, Integer> pair = Pair.make(caller.getMethod()
				.getReference(), site.getProgramCounter());
		IMethod method = factory.get_cha().resolveMethod(site.getDeclaredTarget());
		 
		
		if(method == null)
			return realMethod;
		if (realMethod.isSynthetic()) {
			return realMethod;
		}
		if (method.isSynthetic()) {
			return method;
		}
		/*
		if (actualSig.startsWith("android.os.AsyncTask") || chaSig.startsWith("android.os.AsyncTask") || chaSig.contains("android.os.AsyncTask") || actualSig.contains("android.os.AsyncTask") 
				|| actualSig.startsWith("android.os.Handler") || chaSig.startsWith("android.os.Handler")
				|| actualSig.startsWith("android.os.Message") || chaSig.startsWith("android.os.Message")) {//deal with findViewById in factoryInterpreter
			//found asynctask
			return realMethod;
			
		}*/
		
		TypeReference reference = site.getDeclaredTarget().getReturnType();// default no cast

        IClass retClass = factory.get_cha().lookupClass(reference);
		//FIXME: debug
		if (retClass != null && method != null && (method.getSignature().startsWith("android."))
				&& !(method.getReturnType().equals(TypeReference.Void)) &&(method.getReturnType().isReferenceType()))// android
																		// system
																		// call
		{
			AbstractMethodBuilder builder = new DirectRetNewSummaryMethodBuilder();
			logger.debug("selector: direct new for " + method);
			return factory.findOrCreateMethodFromCache(caller, site, realMethod, pair,builder);
		} else {
			// System.out.println("not my taste, delegate to parent");
			return realMethod;
		}

	}
	

	
	class CallSignature extends Pair<MethodReference, Integer> implements
			Comparable<CallSignature> {

		protected CallSignature(MethodReference fst, Integer snd) {
			super(fst, snd);
		}

		public int compareTo(CallSignature arg0) {
			return 0;
		}
	}
}
