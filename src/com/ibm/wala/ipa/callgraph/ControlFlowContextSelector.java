package com.ibm.wala.ipa.callgraph;
import com.ibm.wala.classLoader.CallSiteReference;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.SyntheticMethod;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.ReceiverInstanceContext;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallString;
import com.ibm.wala.ipa.callgraph.propagation.cfa.CallStringContext;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.intset.IntSet;
import com.ibm.wala.util.intset.SparseIntSet;


/**
 * A context selector selecting a ReciverInstanceContext for java.lang.Thread.start()V invocations.
 */
public class ControlFlowContextSelector implements ContextSelector {
  
    public Context getCalleeTarget(CGNode caller, CallSiteReference site, IMethod callee,  InstanceKey[] actualParameters) {
    if (isControlFlowMethod(callee) && actualParameters != null && actualParameters.length > 0 && actualParameters[0] != null)
      return new ReceiverInstanceContext(actualParameters[0]);
    else
    	return null;
  }

  
  private boolean isControlFlowMethod(IMethod method) {
    IClassHierarchy cha = method.getClassHierarchy();
    //if (method.toString().contains("findViewById"))
    //	return true;
    if (method.isStatic()) {
		return false;
	}
    
    if (cha.isAssignableFrom(cha.lookupClass(TypeReference.JavaLangThread), method.getDeclaringClass())) {
		return true;
	}
    if (cha.isAssignableFrom(cha.lookupClass(TypeReference.findOrCreate(ClassLoaderReference.Primordial, "Landroid/os/Handler")),method.getDeclaringClass())) {
		return true;
	}
    
    //TODO: more elegant way
    
    if (method.toString().contains("setOnClickListener")) {
		return true;
	}
    
    return false;
    /*
    //if (cha.isAssignableFrom(cha.lookupClass(TypeReference.findOrCreate(ClassLoaderReference.Primordial, "Landroid/view/View")),method.getDeclaringClass()))
    {
		if (method.toString().contains("setOnClickListener")) {
			return true;
		}
	}
	*/
  }
  
  public IntSet getRelevantParameters(CGNode caller, CallSiteReference sitesite) {
    if (sitesite.getDeclaredTarget().getNumberOfParameters() > 0)
      return SparseIntSet.singleton(0);
    else 
      return new SparseIntSet();
  }
}