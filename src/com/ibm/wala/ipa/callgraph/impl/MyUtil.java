package com.ibm.wala.ipa.callgraph.impl;

import java.io.InputStream;

import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.ClassTargetSelector;
import com.ibm.wala.ipa.callgraph.ContextSelector;
import com.ibm.wala.ipa.callgraph.MethodTargetSelector;
import com.ibm.wala.ipa.callgraph.propagation.SSAContextInterpreter;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.callgraph.propagation.cfa.ZeroXContainerCFABuilder;
import com.ibm.wala.ipa.callgraph.propagation.cfa.ZeroXInstanceKeys;
import com.ibm.wala.ipa.callgraph.propagation.cfa.nCFABuilder;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.summaries.AndroidMethodTargetSelector;
import com.ibm.wala.ipa.summaries.BypassClassTargetSelector;
import com.ibm.wala.ipa.summaries.BypassMethodTargetSelector;
import com.ibm.wala.ipa.summaries.XMLMethodSummaryReader;
import com.ibm.wala.util.strings.Atom;

public class MyUtil extends Util{
	
	/**
	   * Modify an options object to include bypass logic as specified by a an XML file.
	   * 
	   * @throws IllegalArgumentException if scope is null
	   * @throws IllegalArgumentException if cl is null
	   * @throws IllegalArgumentException if options is null
	   * @throws IllegalArgumentException if scope is null
	   */
	  public static void addBypassLogic(AnalysisOptions options, AnalysisScope scope, ClassLoader cl, String xmlFile,
	      IClassHierarchy cha) throws IllegalArgumentException {
	    if (scope == null) {
	      throw new IllegalArgumentException("scope is null");
	    }
	    if (options == null) {
	      throw new IllegalArgumentException("options is null");
	    }
	    if (cl == null) {
	      throw new IllegalArgumentException("cl is null");
	    }
	    if (cha == null) {
	      throw new IllegalArgumentException("cha cannot be null");
	    }

	    InputStream s = cl.getResourceAsStream(xmlFile);
	    XMLMethodSummaryReader summary = new XMLMethodSummaryReader(s, scope);

	    MethodTargetSelector ms = new BypassMethodTargetSelector(options.getMethodTargetSelector(), summary.getSummaries(), summary
	        .getIgnoredPackages(), cha);
	    options.setSelector(ms);
	    
	    options.setSelector(new AndroidMethodTargetSelector(options.getMethodTargetSelector(),null,null,cha, scope));
	    ClassTargetSelector cs = new BypassClassTargetSelector(options.getClassTargetSelector(), summary.getAllocatableClasses(), cha,
	        cha.getLoader(scope.getLoader(Atom.findOrCreateUnicodeAtom("Synthetic"))));
	    options.setSelector(cs);
	  }
	  
	public static SSAPropagationCallGraphBuilder makeCustomZeroOneContainerCFABuilder(AnalysisOptions options, AnalysisCache cache,
		      IClassHierarchy cha, AnalysisScope scope, ContextSelector appSelector, SSAContextInterpreter appInterpreter) {

		    if (options == null) {
		      throw new IllegalArgumentException("options is null");
		    }
		    MyUtil.addDefaultSelectors(options, cha);
		    MyUtil.addDefaultBypassLogic(options, scope, Util.class.getClassLoader(), cha);
		    options.setUseConstantSpecificKeys(true);

		    return new ZeroXContainerCFABuilder(cha, options, cache, appSelector, appInterpreter, ZeroXInstanceKeys.ALLOCATIONS | ZeroXInstanceKeys.SMUSH_PRIMITIVE_HOLDERS | ZeroXInstanceKeys.SMUSH_THROWABLES | ZeroXInstanceKeys.SMUSH_MANY | ZeroXInstanceKeys.SMUSH_STRINGS);
	}
	
	public static SSAPropagationCallGraphBuilder makeCustomVanillaZeroOneContainerCFABuilder(AnalysisOptions options, AnalysisCache cache,
		      IClassHierarchy cha, AnalysisScope scope, ContextSelector appSelector, SSAContextInterpreter appInterpreter) {

		    if (options == null) {
		      throw new IllegalArgumentException("options is null");
		    }
		    MyUtil.addDefaultSelectors(options, cha);
		    MyUtil.addDefaultBypassLogic(options, scope, Util.class.getClassLoader(), cha);
		    options.setUseConstantSpecificKeys(true);

		    return new ZeroXContainerCFABuilder(cha, options, cache, appSelector, appInterpreter, ZeroXInstanceKeys.ALLOCATIONS | ZeroXInstanceKeys.SMUSH_PRIMITIVE_HOLDERS | ZeroXInstanceKeys.SMUSH_THROWABLES | ZeroXInstanceKeys.SMUSH_MANY);
	}
	
	
	/**
	   * make a {@link CallGraphBuilder} that uses call-string context sensitivity,
	   * with call-string length limited to n, and a context-sensitive
	   * allocation-site-based heap abstraction. Standard optimizations in the heap
	   * abstraction like smushing of strings are disabled.
	   */
	  public static SSAPropagationCallGraphBuilder makeCustomVanillaNCFABuilder(int n, AnalysisOptions options, AnalysisCache cache,
	      IClassHierarchy cha, AnalysisScope scope, ContextSelector appSelector, SSAContextInterpreter appInterpreter) {
	    if (options == null) {
	      throw new IllegalArgumentException("options is null");
	    }
	    addDefaultSelectors(options, cha);
	    addDefaultBypassLogic(options, scope, Util.class.getClassLoader(), cha);
	    SSAPropagationCallGraphBuilder result = new nCFABuilder(n, cha, options, cache, appSelector, appInterpreter);
	    // nCFABuilder uses type-based heap abstraction by default, but we want allocation sites
	    result.setInstanceKeys(new ZeroXInstanceKeys(options, cha, result.getContextInterpreter(), ZeroXInstanceKeys.ALLOCATIONS | ZeroXInstanceKeys.CONSTANT_SPECIFIC | ZeroXInstanceKeys.SMUSH_PRIMITIVE_HOLDERS | ZeroXInstanceKeys.SMUSH_THROWABLES));
	    return result;
	  }

	  public static void addDefaultBypassLogic(AnalysisOptions options, AnalysisScope scope, ClassLoader cl, IClassHierarchy cha) {
		    addBypassLogic(options, scope, cl, nativeSpec, cha);
		  }
}
